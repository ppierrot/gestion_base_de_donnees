1/
mysql> select villedep as 'liste des villes'
    -> from etape
    -> where date between '2014-01-01' and '2014-07-19'
    -> union
    -> select villear
    -> from etape
    -> where nbkm<150
    -> union
    -> select ville
    -> from coureur
    -> where numequipe = 4
    -> order by 'liste des villes';
+------------------+
| liste des villes |
+------------------+
| Le Touquet       |
| Arras            |
| Tomblaine        |
| Gerardmer        |
| Saint-Etienne    |
| Hautacam         |
| Perigueux        |
| Paris            |
| Lille            |
| Mulhouse         |
+------------------+

2/
mysql> select numcoureur from resultat where prime=200 union select numcoureur from coureur where salaire between 3001 and 3999;
+------------+
| numcoureur |
+------------+
|          1 |
|          3 |
|          5 |
|          7 |
|          9 |
+------------+

3/
mysql> select numcoureur from coureur where salaire between 3001 and 3999 and numcoureur in (select numcoureur from resultat where prime=350);
+------------+
| numcoureur |
+------------+
|          3 |
|          9 |
+------------+

4/
mysql> select distinct numequipe from coureur where salaire >= 3500 and numequipe in ( select numequipe from equipe where nomdirecteur = 'Bonvalet');

5/
mysql> select typeetape from type where numtype not in (select numtype from etape where type.numtype=etape.numtype);
+-----------------------------+
| typeetape                   |
+-----------------------------+
| contre la montre par equipe |
+-----------------------------+

6/
mysql> select numetape from etape where numetape not in (select numetape from resultat);
+----------+
| numetape |
+----------+
|        5 |
|        1 |
|        2 |
|        3 |
|        4 |
+----------+

7/
mysql> select numcoureur from coureur where numcoureur not in (select numcoureur from resultat where rang=1);
+------------+
| numcoureur |
+------------+
|          4 |
|          5 |
|          7 |
|          3 |
|          9 |
|          1 |
|          2 |
+------------+

8/
Select numetape from etape where numetape not in (select numetape from resultat where prime IS NOT NULL);

+----------+
| numetape |
+----------+
|        5 |
|        6 |
|        1 |
|        2 |
|        3 |
|        4 |
+----------+

9/
mysql> select ville from coureur where ville not in (select villedep from etape union select villear from etape);
+--------+
| ville  |
+--------+
| Annecy |
+--------+

10/
mysql> select * from equipe where numequipe not in (select numequipe from coureur);
+-----------+-----------+--------------+
| numequipe | nomequipe | nomdirecteur |
+-----------+-----------+--------------+
|         5 | Cofidis   | Petit        |
+-----------+-----------+--------------+

11/
mysql> select nomcoureur from coureur where nomcoureur not in (select numcoureur from resultat where numetape=7);
+------------+
| nomcoureur |
+------------+
| Pinot      |
| Pineau     |
| Nibali     |
| Valverde   |
| Chavanel   |
| Vockler    |
| Rolland    |
| Sagan      |
| Petit      |
+------------+

12/
mysql> select nomcoureur,prenom from coureur where nomcoureur not in (select nomcoureur from coureur where ville='mulhouse') and salaire=4000;
+------------+---------+
| nomcoureur | prenom  |
+------------+---------+
| Pinot      | Thibaut |
| Pineau     | Cedric  |
+------------+---------+

13/
mysql> select nomequipe from equipe where numequipe not in (select numequipe from coureur where numcoureur in (select numcoureur from resultat));
+-----------+
| nomequipe |
+-----------+
| Rabobank  |
| Cofidis   |
+-----------+

14/
mysql> select numetape,round(nbkm/0.95) from etape;
+----------+------------------+
| numetape | round(nbkm/0.95) |
+----------+------------------+
|        1 |              172 |
|        2 |              204 |
|        3 |              169 |
|        4 |              179 |
|        5 |              176 |
|        6 |              153 |
|        7 |               57 |
|        8 |              144 |
+----------+------------------+

15/
mysql> select day(current_date) as 'jour', month(current_date) as 'mois', year(current_date) as 'année';
+------+------+-------+
| jour | mois | année |
+------+------+-------+
|    2 |   12 |  2016 |
+------+------+-------+