#accès à MySQL via Commande Prompt Windows
#si ça marche pas, ouvrir le gestionnaire de tâche et regarder dans l'onglet "services" si "mysql" est bien en cours d'excution
set PATH=%PATH%;C:\Program Files\MySQL\MySQL Server 5.7\bin
mysql -u root -p
#touche entrée

create database td2;
use td2;
create table coureur (
         numlicence int primary key not null auto_increment,
         nomcoureur varchar(10),
         prenom varchar(10),
         datenais date #time 
         );

insert into coureur values (1, 'quinqueton','joel', '19621129');
...

#Pour modifier les données d'une colonne :
alter table [nom de la table] modify [colonne] [données à modifier];
#Poour modifier une valeur dans une colonne :
update [table] set [valeur à modifier] where [clé(s) primaire(s) référencant la ligne]
#pour modifier une colonne en clé étrangère, faire :
alter table [nom de la table] ADD [procédure clé étrangère]

#Pour supprimer un enregistrement précis dans une table :
DELETE FROM [table] WHERE [champ]=[enregistrement];

#Pour renommer un champs :
alter table [table] change [ancien nom] [nouveau nom] [type de donnée]

#pour connaitre les domaines d'une table : 
describle [table];

#Pour clé étrangère :
constraint fk_[nom de la clé] foreign key (colonne choisit dans la table) references [table primaire](colonne primaire);

select * from coureur; 
+------------+------------+---------+------------+
| numlicence | nomcoureur | prenom  | datenais   |
+------------+------------+---------+------------+
|          1 | quinqueton | joel    | 1962-11-29 |
|          2 | berry      | pierre  | NULL       |
|          3 | boe        | noemie  | 1972-08-18 |
|          4 | durand     | sylvain | 1970-06-30 |
|          5 | lochard    | sophie  | 1968-06-15 |
+------------+------------+---------+------------+

select * from course;

+-----------+------------+------------+----------+
| numcourse | datecourse | codepostal | ville    |
+-----------+------------+------------+----------+
|       201 | 2012-11-29 |      75000 | paris    |
|       202 | 2012-11-05 |      80330 | longueau |
|       203 | 2013-06-15 |      59000 | lille    |
|       204 | 2013-06-30 |      60000 | beauvais |
|       205 | 2013-08-18 |      75000 | paris    |
|       206 | 2013-09-03 |      80000 | amiens   |
+-----------+------------+------------+----------+


select * from resultat;

+-----------+------------+----------+------+
| numcourse | numlicence | temps    | rang |
+-----------+------------+----------+------+
|       204 |          1 | 01:25:00 |   15 |
|       202 |          2 | 01:20:00 |    6 |
|       202 |          3 | 01:35:00 |   12 |
|       203 |          4 | 01:35:00 |    8 |
|       203 |          5 | 01:40:00 |   15 |
|       204 |          2 | 01:15:00 |    2 |
|       203 |          1 | 01:55:00 |   20 |
+-----------+------------+----------+------+
1/
 select nomcoureur, prenom, datenais
    -> from coureur;
+------------+---------+------------+
| nomcoureur | prenom  | datenais   |
+------------+---------+------------+
| quinqueton | joel    | 1962-11-29 |
| berry      | pierre  | NULL       |
| boe        | noemie  | 1972-08-18 |
| durand     | sylvain | 1970-06-30 |
| lochard    | sophie  | 1968-06-15 |
+------------+---------+------------+
2/
select datecourse, ville
    -> from course;
+------------+----------+
| datecourse | ville    |
+------------+----------+
| 2012-11-29 | paris    |
| 2012-11-05 | longueau |
| 2013-06-15 | lille    |
| 2013-06-30 | beauvais |
| 2013-08-18 | paris    |
| 2013-09-03 | amiens   |
+------------+----------+
3/
select numcourse, numlicence, temps
    -> from resultat
    -> where numcourse = 203;
+-----------+------------+----------+
| numcourse | numlicence | temps    |
+-----------+------------+----------+
|       203 |          4 | 01:35:00 |
|       203 |          5 | 01:40:00 |
|       203 |          1 | 01:55:00 |
+-----------+------------+----------+
4/
select numcourse, datecourse
    -> from course
    -> where ville='paris';
+-----------+------------+
| numcourse | datecourse |
+-----------+------------+
|       201 | 2012-11-29 |
|       205 | 2013-08-18 |
+-----------+------------+
5/
select nomcoureur
    -> from coureur
    -> where datenais < '19700101';
+------------+
| nomcoureur |
+------------+
| quinqueton |
| lochard    |
+------------+
6/
 select numcourse, numlicence
    -> from resultat
    -> where rang between 10 and 15;

select numcourse, numlicence
    -> from resultat
    -> where rang >= 10 
       and rang <= 15;
+-----------+------------+
| numcourse | numlicence |
+-----------+------------+
|       204 |          1 |
|       202 |          3 |
|       203 |          5 |
+-----------+------------+
7/
select numcourse, ville
    -> from course
    -> where datecourse between '20121101' 
       and '20121130';

select numcourse, ville
    -> from course
    -> where datecourse >= '20121101' 
       and datecourse <= '20121130';

+-----------+----------+
| numcourse | ville    |
+-----------+----------+
|       201 | paris    |
|       202 | longueau |
+-----------+----------+
8/
select nomcoureur
    -> from coureur
    -> where datenais is null;
+------------+
| nomcoureur |
+------------+
| berry      |
+------------+
9/
select numcourse
    -> from course
    -> where ville like 'L%';
+-----------+
| numcourse |
+-----------+
|       202 |
|       203 |
+-----------+
10/
select nomcoureur, prenom, datenais
    -> from coureur
    -> order by datenais desc;
+------------+---------+------------+
| nomcoureur | prenom  | datenais   |
+------------+---------+------------+
| boe        | noemie  | 1972-08-18 |
| durand     | sylvain | 1970-06-30 |
| lochard    | sophie  | 1968-06-15 |
| quinqueton | joel    | 1962-11-29 |
| berry      | pierre  | NULL       |
+------------+---------+------------+
11/
select ville, datecourse
    -> from course
    -> order by ville asc, datecourse desc;
+----------+------------+
| ville    | datecourse |
+----------+------------+
| amiens   | 2013-09-03 |
| beauvais | 2013-06-30 |
| lille    | 2013-06-15 |
| longueau | 2012-11-05 |
| paris    | 2013-08-18 |
| paris    | 2012-11-29 |
+----------+------------+
12/
select distinct numlicence
    -> from resultat
    -> where numcourse = 202 or 204
    -> order by numlicence;
+------------+
| numlicence |
+------------+
|          1 |
|          2 |
|          3 |
|          4 |
|          5 |
+------------+
13/
select numcourse, datecourse, ville
    -> from course
    -> where ville <> 'paris'
    -> and datecourse between '20121101' and '20121130';
+-----------+------------+----------+
| numcourse | datecourse | ville    |
+-----------+------------+----------+
|       202 | 2012-11-05 | longueau |
+-----------+------------+----------+
14/
select numcourse, temps, numlicence
    -> from resultat
    -> where numlicence = 2;
+-----------+----------+------------+
| numcourse | temps    | numlicence |
+-----------+----------+------------+
|       202 | 01:20:00 |          2 |
|       204 | 01:15:00 |          2 |
+-----------+----------+------------+
15/
select numcourse, temps
    -> from resultat, coureur
    -> where coureur.numlicence = resultat.numlicence
    -> and nomcoureur = 'Lochard';
+-----------+----------+
| numcourse | temps    |
+-----------+----------+
|       203 | 01:40:00 |
+-----------+----------+
16/
select numcourse, nomcoureur, temps, rang
    -> from coureur, resultat
    -> where coureur.numlicence = resultat.numlicence;
+-----------+------------+----------+------+
| numcourse | nomcoureur | temps    | rang |
+-----------+------------+----------+------+
|       204 | quinqueton | 01:25:00 |   15 |
|       202 | berry      | 01:20:00 |    6 |
|       202 | boe        | 01:35:00 |   12 |
|       203 | durand     | 01:35:00 |    8 |
|       203 | lochard    | 01:40:00 |   15 |
|       204 | berry      | 01:15:00 |    2 |
|       203 | quinqueton | 01:55:00 |   20 |
+-----------+------------+----------+------+
17/
select rang, temps, numlicence, resultat.numcourse
    -> from resultat, course
    -> where resultat.numcourse = course.numcourse
    -> and ville = 'lille';
+------+----------+------------+-----------+
| rang | temps    | numlicence | numcourse |
+------+----------+------------+-----------+
|    8 | 01:35:00 |          4 |       203 |
|   15 | 01:40:00 |          5 |       203 |
|   20 | 01:55:00 |          1 |       203 |
+------+----------+------------+-----------+
18/
select temps, rang, nomcoureur, resultat.numcourse
    -> from resultat, course, coureur
    -> where resultat.numcourse = course.numcourse
    -> and resultat.numlicence = coureur.numlicence
    -> and datecourse between '20130601' and '20130630';
+----------+------+------------+-----------+
| temps    | rang | nomcoureur | numcourse |
+----------+------+------------+-----------+
| 01:25:00 |   15 | quinqueton |       204 |
| 01:35:00 |    8 | durand     |       203 |
| 01:40:00 |   15 | lochard    |       203 |
| 01:15:00 |    2 | berry      |       204 |
| 01:55:00 |   20 | quinqueton |       203 |
+----------+------+------------+-----------+
19/
select rang, temps, nomcoureur, prenom, ville
    -> from resultat, coureur, course
    -> where resultat.numlicence = coureur.numlicence
    -> and resultat.numcourse = course.numcourse
    -> and temps < '01:30:00'
    -> and codepostal like '80%';
+------+----------+------------+--------+----------+
| rang | temps    | nomcoureur | prenom | ville    |
+------+----------+------------+--------+----------+
|    6 | 01:20:00 | berry      | pierre | longueau |
+------+----------+------------+--------+----------+
