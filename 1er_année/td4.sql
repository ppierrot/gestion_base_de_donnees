1
mysql> select villedep, villear
    -> from etape, type
    -> where typeetape='plaine'
    -> and etape.numtype=type.numtype;

mysql> select villedep,villear
    -> from etape
    -> where numtype in (select numtype from type where typeetape='plaine');
+------------+---------+
| villedep   | villear |
+------------+---------+
| Le Touquet | Lille   |
| Arras      | Reims   |
| Evry       | Paris   |
+------------+---------+

2
mysql> select rang, numcoureur, temps
    -> from resultat, etape
    -> where villear='Paris'
    -> and
    -> resultat.numetape=etape.numetape
    -> order by rang;

mysql> select rang,numcoureur,temps
    -> from resultat
    -> where numetape in 
    ->(select numetape from etape where villear='Paris')
    -> order by rang;

+------+------------+----------+
| rang | numcoureur | temps    |
+------+------------+----------+
|    1 |          8 | 03:20:00 |
|    2 |          9 | 03:20:01 |
|    3 |          1 | 03:20:02 |
|    5 |          2 | 03:20:13 |
+------+------------+----------+

3
mysql> select nomcoureur, prenom
    -> from coureur, resultat, etape
    -> where date ='2014-07-27'
    -> and
    -> resultat.numetape=etape.numetape
    -> and
    -> coureur.numcoureur=resultat.numcoureur;

mysql> select nomcoureur,prenom
    -> from coureur
    -> where numcoureur in (select numcoureur from resultat where resultat.numetape in(select numetape from etape where date='2014-07-27');

+------------+---------+
| nomcoureur | prenom  |
+------------+---------+
| Pinot      | Thibaut |
| Pineau     | Cedric  |
| Sagan      | Peter   |
| Petit      | Adrien  |
+------------+---------+

4
mysql> select nomcoureur,prenom
    -> from coureur,resultat,etape
    -> where date ='2014-07-27'
    -> and
    -> resultat.numetape=etape.numetape
    -> and
    -> rang=1
    -> and
    -> coureur.numcoureur=resultat.numcoureur;

mysql> select nomcoureur, prenom
    -> from coureur
    -> where numcoureur in
    -> (select numcoureur from resultat, etape where rang=1 and resultat.numetape=etape.numetape and date='2014-07-27');

+------------+--------+
| nomcoureur | prenom |
+------------+--------+
| Sagan      | Peter  |
+------------+--------+

5 **** revoir version imbriquee : revu
mysql> select nomcoureur, prenom
    -> from resultat,etape,coureur,equipe
    -> where coureur.numequipe=equipe.numequipe
    -> and
    -> nomequipe='AG2R'
    -> and
    -> date='2014-07-27'
    -> and
    -> resultat.numetape=etape.numetape
    -> and
    -> resultat.numcoureur=coureur.numcoureur;

mysql> select nomcoureur,prenom
    -> from coureur
    -> where numequipe in (select numequipe from equipe where nomequipe='AG2R')
    -> and numcoureur in (select numcoureur from resultat,etape where resultat.numetape=etape.numetape and date='2014-07-27');

+------------+---------+
| nomcoureur | prenom  |
+------------+---------+
| Pinot      | Thibaut |
| Pineau     | Cedric  |
+------------+---------+

6**** imbriquee : fait
mysql> select distinct r1.numcoureur
    -> from resultat as r1, resultat as r2
    -> where r2.numcoureur=3
    -> and
    -> r1.temps<r2.temps;

mysql> select numcoureur
    -> from resultat
    -> where temps<any(select temps from resultat where numcoureur=3);
+------------+
| numcoureur |
+------------+
|          1 |
|          2 |
|          3 |
|          6 |
|          8 |
|          9 |
+------------+

7 ****idem : fait
mysql> select distinct e1.numetape
    -> from etape as e1, etape as e2
    -> where e2.numtype=3
    -> and
    -> e1.nbkm>e2.nbkm;

mysql> select numetape
    -> from etape
    -> where nbkm>any(select nbkm from etape where numtype=3);

+----------+
| numetape |
+----------+
|        1 |
|        2 |
|        3 |
|        4 |
|        5 |
+----------+

8 ****non faire imbriquee avec all
mysql> select distinct e1.numetape
    -> from etape as e1, etape as e2, type
    -> where typeetape='montagne'
    -> and
    -> e2.numtype=e1.numtype
    -> and
    -> e1.nbkm>e2.nbkm;
+----------+
| numetape |
+----------+
|        2 |
|        4 |
|        5 |
|        1 |
+----------+

9
mysql> select r1.numcoureur
    -> from resultat as r1, resultat as r2
    -> where r2.numcoureur=2
    -> and
    -> r1.numcoureur<>2
    -> and
    -> r1.numetape=r2.numetape;

mysql> select numcoureur
    -> from resultat
    -> where numcoureur <>2
    -> and numetape in (select numetape from resultat where numcoureur=2);
    
+------------+
| numcoureur |
+------------+
|          1 |
|          8 |
|          9 |
+------------+

10
mysql> select r1.numcoureur
    -> from resultat as r1, resultat as r2
    -> where r1.temps<r2.temps
    -> and
    -> r1.numetape=r2.numetape
    -> and
    -> r2.numcoureur=3;

+------------+
| numcoureur |
+------------+
|          6 |
+------------+

11 *** faire double jointure cf cours sgbdr2016 : fait
mysql> select r1.numcoureur
    -> from resultat as r1, resultat as r2
    -> where r1.numcoureur<>3
    -> and
    -> r2.numcoureur=3
    -> and
    -> (r1.rang,r1.prime)=(r2.rang,r2.prime);

mysql>select numcoureur 
     >from resultat 
     >where numcoureur<>3 
     >and (rang,prime) in (select rang,prime from resultat where numcoureur = 3);

+------------+
| numcoureur |
+------------+
|          9 |
+------------+

12
mysql> select nomcoureur
    -> from coureur, etape
    -> where ville=villedep
    -> ;
+------------+
| nomcoureur |
+------------+
| Pinot      |
| Rolland    |
| Petit      |
| Chavanel   |
| Sagan      |
+------------+

13
mysql> select nomcoureur
    -> from coureur
    -> where ville<>any(select villedep from etape);
+------------+
| nomcoureur |
+------------+
| Pinot      |
| Pineau     |
| Nibali     |
| Valverde   |
| Chavanel   |
| Vockler    |
| Rolland    |
| Sagan      |
| Petit      |
+------------+

14 
mysql> select distinct nomcoureur
    -> from coureur, etape, resultat
    -> where etape.villedep=coureur.ville
    -> and
    -> resultat.numcoureur=coureur.numcoureur
    -> and
    -> etape.numetape=resultat.numetape;
+------------+
| nomcoureur |
+------------+
| Sagan      |
+------------+

15
mysql> select *
    -> from coureur
    -> left join resultat on coureur.numcoureur=resultat.numcoureur;
+------------+------------+-----------+-----------+---------+-----------+------------+----------+----------+------+-------+
| numcoureur | nomcoureur | prenom    | ville     | salaire | numequipe | numcoureur | numetape | temps    | rang | prime |
+------------+------------+-----------+-----------+---------+-----------+------------+----------+----------+------+-------+
|          1 | Pinot      | Thibaut   | Arras     |    4000 |         4 |          1 |        8 | 03:20:02 |    3 |   200 |
|          2 | Pineau     | Cedric    | Lille     |    4000 |         4 |          2 |        8 | 03:20:13 |    5 |  NULL |
|          3 | Nibali     | Vincenzo  | Annecy    |    3200 |         3 |          3 |        6 | 03:30:13 |    5 |  NULL |
|          3 | Nibali     | Vincenzo  | Annecy    |    3200 |         3 |          3 |        7 | 01:12:01 |    2 |   350 |
|          4 | Valverde   | Alejandro | Paris     |    2500 |         1 |          4 |        6 | 06:40:00 |   10 |  NULL |
|          5 | Chavanel   | Sylvain   | Pau       |    3700 |         2 |       NULL |     NULL | NULL     | NULL |  NULL |
|          6 | Vockler    | Thomas    | Mulhouse  |    4000 |         4 |          6 |        7 | 01:10:00 |    1 |   500 |
|          7 | Rolland    | Pierre    | Gerardmer |    3700 |         2 |       NULL |     NULL | NULL     | NULL |  NULL |
|          8 | Sagan      | Peter     | Evry      |    3000 |         1 |          8 |        8 | 03:20:00 |    1 |   500 |
|          9 | Petit      | Adrien    | Gerardmer |    3200 |         3 |          9 |        8 | 03:20:01 |    2 |   350 |
+------------+------------+-----------+-----------+---------+-----------+------------+----------+----------+------+-------+

#on éssaye de voir quels coureurs n'ont pas particpé à des courses.


mysql> select *
    -> from resultat right join etape on resultat.numetape=etape.numetape;
+------------+----------+----------+------+-------+----------+------------+---------------+------------+------+---------+
| numcoureur | numetape | temps    | rang | prime | numetape | date       | villedep      | villear    | nbkm | numtype |
+------------+----------+----------+------+-------+----------+------------+---------------+------------+------+---------+
|          1 |        8 | 03:20:02 |    3 |   200 |        8 | 2014-07-27 | Evry          | Paris      |  137 |       4 |
|          2 |        8 | 03:20:13 |    5 |  NULL |        8 | 2014-07-27 | Evry          | Paris      |  137 |       4 |
|          3 |        6 | 03:30:13 |    5 |  NULL |        6 | 2014-07-24 | Pau           | Hautacam   |  145 |       3 |
|          3 |        7 | 01:12:01 |    2 |   350 |        7 | 2014-07-26 | Bergerac      | Perigueux  |   54 |       1 |
|          4 |        6 | 06:40:00 |   10 |  NULL |        6 | 2014-07-24 | Pau           | Hautacam   |  145 |       3 |
|          6 |        7 | 01:10:00 |    1 |   500 |        7 | 2014-07-26 | Bergerac      | Perigueux  |   54 |       1 |
|          8 |        8 | 03:20:00 |    1 |   500 |        8 | 2014-07-27 | Evry          | Paris      |  137 |       4 |
|          9 |        8 | 03:20:01 |    2 |   350 |        8 | 2014-07-27 | Evry          | Paris      |  137 |       4 |
|       NULL |     NULL | NULL     | NULL |  NULL |        1 | 2014-07-05 | Le Touquet    | Lille      |  163 |       4 |
|       NULL |     NULL | NULL     | NULL |  NULL |        2 | 2014-07-10 | Arras         | Reims      |  194 |       4 |
|       NULL |     NULL | NULL     | NULL |  NULL |        3 | 2014-07-12 | Tomblaine     | Gerardmer  |  161 |       5 |
|       NULL |     NULL | NULL     | NULL |  NULL |        4 | 2014-07-13 | Gerardmer     | Mulhouse   |  170 |       5 |
|       NULL |     NULL | NULL     | NULL |  NULL |        5 | 2014-07-18 | Saint-Etienne | Chamrousse |  167 |       3 |
+------------+----------+----------+------+-------+----------+------------+---------------+------------+------+---------+

#On éssaye de voir quelles étapes n'ont vu aucun des coureurs concourrir sur ces derniers