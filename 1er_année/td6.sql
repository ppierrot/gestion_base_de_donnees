1/
mysql> select min(salaire),avg(salaire),max(salaire) from coureur group by salaire;
+--------------+--------------+--------------+
| min(salaire) | avg(salaire) | max(salaire) |
+--------------+--------------+--------------+
|         2500 |    2500.0000 |         2500 |
|         3000 |    3000.0000 |         3000 |
|         3200 |    3200.0000 |         3200 |
|         3700 |    3700.0000 |         3700 |
|         4000 |    4000.0000 |         4000 |
+--------------+--------------+--------------+

2/
mysql> select avg(prime) from resultat;
+------------+
| avg(prime) |
+------------+
|   380.0000 |
+------------+

3/
mysql> select numetape, avg(prime) from resultat group by numetape;
+----------+------------+
| numetape | avg(prime) |
+----------+------------+
|        6 |       NULL |
|        7 |   425.0000 |
|        8 |   350.0000 |
+----------+------------+

4/ mysql> select numequipe, avg(prime) as primeq from resultat where numcoureur in (select numcoureur from coureur) group by numequipe;

5/
mysql> select numtype, avg(nbkm) from etape group by numtype;
+---------+-----------+
| numtype | avg(nbkm) |
+---------+-----------+
|       1 |   54.0000 |
|       3 |  156.0000 |
|       4 |  164.6667 |
|       5 |  165.5000 |
+---------+-----------+

6/mysql> select etape, avg(nbkm) as 'moykm' from type where numtype in (select numtype from etape) group by numtype, typetape;

7/mysql> select typeetape, sum(nbkm) as 'somme' from etape where numtype in (select numtype from type) group by numtype;

8/
mysql> select count(numcoureur) from resultat where numetape=8;
+-------------------+
| count(numcoureur) |
+-------------------+
|                 4 |
+-------------------+

9/mysql> select numequipe, count(numcoureur),count(numetape) from coureur where numcoureur in(select numcoureur from resultat) group by numequipe;

10/mysql> select numequipe, avg(rang) from coureur where numcoureur in (select numcoureur from resultat) group by numequipe;

11/mysql> select numequipe, nomcoureur, avg(rang) from resultat where numcoureur in (select numcoureur from coureur) group by numequipe, numcoureur order by avg(rang);

12/
mysql> select numequipe, count(distinct coureur.numcoureur) as 'coureur engagés' from coureur where  numcoureur in  (select numcoureur from resultat where prime is not NULL) group by numequipe;
+-----------+-----------------+
| numequipe | coureur engagés |
+-----------+-----------------+
|         1 |               1 |
|         3 |               2 |
|         4 |               2 |
+-----------+-----------------+

13/
mysql> select numetape, villedep, villear from etape where nbkm in (select min(nbkm) from etape);
+----------+----------+-----------+
| numetape | villedep | villear   |
+----------+----------+-----------+
|        7 | Bergerac | Perigueux |
+----------+----------+-----------+

14/
mysql> select numcoureur, nomcoureur from coureur where salaire in (select max(salaire) from coureur);
+------------+------------+
| numcoureur | nomcoureur |
+------------+------------+
|          1 | Pinot      |
|          2 | Pineau     |
|          6 | Vockler    |
+------------+------------+

15/
mysql> select numequipe, count(numcoureur) as 'nbr coureurs' from coureur where numcoureur in (select numcoureur from resultat) group by numequipe having count(distinct coureur.numcoureur)>2;
+-----------+--------------+
| numequipe | nbr coureurs |
+-----------+--------------+
|         4 |            3 |
+-----------+--------------+

16/select numequipe from resultat where numcoureur in (select numcoureur from coureur) group by numequipe having avg(prime) > (select avg(prime) from resultat);

17/
mysql> select hour(temps), count(numetape) from resultat group by hour(temps);
+-------------+-----------------+
| hour(temps) | count(numetape) |
+-------------+-----------------+
|           1 |               2 |
|           3 |               4 |
|           6 |               2 |
+-------------+-----------------+
