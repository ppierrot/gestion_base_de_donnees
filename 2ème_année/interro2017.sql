sportif(NUMSP,nom,prenom,quartier,categorie)
epreuve(CODE,intitule,lieu,taille_max_eq)
inscription(NUM_INSCR,code_ep,dossard)
compo_equipe(NUM_INSCR,NUMSP,date_inscr,role)
podium(CODE_EP,PLACE,num_inscr)

create or replace procedure InscripSportif(ninscr IN number(4), nsportif IN number(4), nouv_taille OUT number)
is 
  nbr_membre_equip number;
  taille_equipe number(2);
  BEGIN
    select count(num_inscr)
    into nbr_membre_equip
    from compo_equipe
    where num_inscr = ninscr
    ;
  
    select taille_max_eq
    into taille_equipe
    from inscription ins join epreuve ep on ins.num_inscr = ep.code
    where num_inscr = ninscr
    ;

    if nbr_membre_equip < taille_equipe
    then 
      insert into compo_equipe(num_inscr,numps,date_inscr,role)
      values (ninscr,nsportif,datesys,NULL)
      ;
      nouv_taille := nbr_membre_equip + 1
      ;
    else
      DBMS_OUTPUT.PUT_LINE("Inscription refusée")
      ;
    end if;
  END;

grant execute
on InscripSportif
to dupont

create or replace package Gestion_resultats
is
  function PointsSporti(nsportif number(4)) return points number;
end Gestion_resultats;

create or replace package body Gestion_resultats
is
  function PointsSporti(nsportif number(4)) 
    return points number;
  is
    cursor curseur is
      select place 
      from sportif f join on compo_equipe c on f.numsp = c.numsp join on podium p on c.num_inscr = p.num_inscr
      where numsp = nsportif
      ;
  begin
    for rslt in curseur loop
      points := round(points+(9/rslt))
    end loop;
    return points;
  end PointsSporti;
end Gestion_resultats;

