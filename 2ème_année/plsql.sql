Nom                                       NULL ?   Type
----------------------------------------- -------- ----------------------------
EMPNO                                     NOT NULL NUMBER(5)
ENAME                                     NOT NULL VARCHAR2(20)
JOB                                                VARCHAR2(20)
MGR                                                NUMBER(5)
HIREDATE                                           DATE
SAL                                       NOT NULL NUMBER(8,2)
COMM                                               NUMBER(8)
DEPTNO                                             NUMBER(2)

/*2/Ecrire une procédure stockée augmentMontant(employe, montant) qui permet d'augmenter l'employé de montant.*/

CREATE OR REPLACE PROCEDURE augmentMontant(employe NUMBER, montant NUMBER) 
IS 
  newSalaire NUMBER;
BEGIN
  SELECT SAL + montant INTO newSalaire
  FROM emp
  WHERE empno = employe;

  UPDATE emp
  SET sal = newSalaire
  WHERE empno = employe;

END augmentMontant;
/

/*6. Ecrire une procédure stockée afficheSalEmp(employe) qui permet d'afficher à l'écran le salaire
d'un employé. Testez. */

CREATE OR REPLACE PROCEDURE afficheSalEmp(employe NUMBER)
IS
  currentSalaire NUMBER;
BEGIN
  SELECT sal
  INTO currentSalaire
  FROM emp
  WHERE empno = employe
  ;

  DBMS_OUTPUT.PUT_LINE(currentSalaire);
END afficheSalEmp;
/

/*7. Ecrire une procédure stockée annuaire qui affiche tous les employés : nom, fonction et département. Testez.*/

SET SERVEROUTPUT ON

CREATE or REPLACE PROCEDURE annuaire
IS
  CURSOR c_emp IS
    SELECT ENAME||' '||nvl(JOB, 'undefined')||' '||to_char(DEPTNO) affichage
    FROM emp
    ;

BEGIN
  FOR emp_rec in c_emp LOOP

    DBMS_OUTPUT.PUT_LINE(emp_rec.affichage);

  END LOOP;

END annuaire;
/

/*8. Ecrire une procédure stockée augmentPourcent(employe, pourcent, nouvSal) qui
permet d'augmenter l'employé de pourcent% et affecte au dernier paramètre la nouvelle
valeur. Syntaxe : nomparam mode type.*/7

SET SERVEROUTPUT ON

CREATE or REPLACE PROCEDURE augmentPourcent(employe IN NUMBER, pourcent IN NUMBER, nouvSal OUT NUMBER)
IS
BEGIN
  UPDATE emp
  SET SAL = SAL + (SAL*(pourcent/100))
  WHERE EMPNO = employe
  ;
  SELECT SAL
  INTO nouvSal
  FROM emp
  WHERE EMPNO = employe
  ;
END augmentPourcent;
/

variable affichage number; execute augmentPourcent(7369,5,:affichage); exec DBMS_OUTPUT.PUT_LINE(:affichage);

/*10. Ecrire une fonction stockée revenuAnnuel(employe) qui retourne le revenu annuel de l'employé.*/

SET SERVEROUTPUT ON

CREATE OR REPLACE FUNCTION revenuAnnuel(employe IN NUMBER)
RETURN NUMBER
IS
  sal_year NUMBER;
  BEGIN
    SELECT SAL*12
    INTO sal_year
    FROM emp
    WHERE EMPNO = employe
    ;
    return sal_year;
  END revenuAnnuel;
/

 select revenuAnnuel(7369) from dual;

/*12. Ecrire une fonction stockée revenuAnnuelPromoAnc(employe) qui retourne le revenu annuel de l'employé,
en lui ajoutant un 13e mois égal au salaire quand son ancienneté est inférieure à 10 ans,
égal au 13e mois augmenté d'un pourcentage égal à son ancienneté quand celle-ci est supérieure ou égale à 10 ans. Testez.*/

SET SERVEROUTPUT ON

CREATE OR REPLACE FUNCTION revenuAnnuelPromoAnc(employe IN NUMBER)
RETURN NUMBER
IS
  engage DATE;
  sal_year NUMBER;
  salaire number;73
  BEGIN
    SELECT HIREDATE, sal
    INTO engage, salaire
    FROM emp
    WHERE EMPNO = employe
    ;

    IF engage < ADD_MONTHS(sysdate,-12)
    THEN sal_year := salaire * 13
    ;
    ELSE sal_year := salaire * 13 + (salaire*((sysdate - engage)/100))
    ;
    END IF;
    RETURN sal_year;
    END revenuAnnuelPromoAnc;
/  

select revenuAnnuelPromoAnc(7369) from dual;

/*13. Mettre ces sous-programmes dans un package GestionFinanciere.*/

SET SERVEROUTPUT ON

CREATE OR REPLACE PACKAGE GestionFinanciere
IS
  PROCEDURE augmentMontant(employe NUMBER, montant NUMBER);
  PROCEDURE afficheSalEmp(employe NUMBER);
  PROCEDURE annuaire;
  PROCEDURE augmentPourcent(employe IN NUMBER, pourcent IN NUMBER, nouvSal OUT NUMBER);
  FUNCTION revenuAnnuel(employe IN NUMBER) RETURN NUMBER;
  FUNCTION revenuAnnuelPromoAnc(employe IN NUMBER) RETURN NUMBER;
END GestionFinanciere;
/

CREATE OR REPLACE PACKAGE BODY GestionFinanciere 
IS
  PROCEDURE augmentMontant(employe NUMBER, montant NUMBER) IS
  BEGIN
    SELECT SAL + montant INTO newSalaire
    FROM emp
    WHERE empno = employe;

    UPDATE emp
    SET sal = newSalaire
    WHERE empno = employe;

  END augmentMontant;

  PROCEDURE afficheSalEmp(employe NUMBER) IS
    currentSalaire NUMBER;
  BEGIN
    SELECT sal
    INTO currentSalaire
    FROM emp
    WHERE empno = employe
    ;

    DBMS_OUTPUT.PUT_LINE(currentSalaire);
  END afficheSalEmp;

  PROCEDURE annuaire IS
  CURSOR c_emp IS
    SELECT ENAME||' '||nvl(JOB, 'undefined')||' '||to_char(DEPTNO) affichage
    FROM emp
    ;

  BEGIN
    FOR emp_rec in c_emp LOOP

      DBMS_OUTPUT.PUT_LINE(emp_rec.affichage);

    END LOOP;

  END annuaire;

  PROCEDURE augmentPourcent(employe IN NUMBER, pourcent IN NUMBER, nouvSal OUT NUMBER)
  IS
  BEGIN
    UPDATE emp
    SET SAL = SAL + (SAL*(pourcent/100))
    WHERE EMPNO = employe
    ;
    SELECT SAL
    INTO nouvSal
    FROM emp
    WHERE EMPNO = employe
    ;
  END augmentPourcent;

  FUNCTION revenuAnnuel(employe IN NUMBER)
  RETURN NUMBER
  IS
  sal_year NUMBER;
  BEGIN
    SELECT SAL*12
    INTO sal_year
    FROM emp
    WHERE EMPNO = employe
    ;
    return sal_year;
  END revenuAnnuel;

  FUNCTION revenuAnnuelPromoAnc(employe IN NUMBER)
  RETURN NUMBER
  IS
    engage DATE;
    sal_year NUMBER;
    salaire number;
  BEGIN
    SELECT HIREDATE, sal
    INTO engage, salaire
    FROM emp
    WHERE EMPNO = employe
    ;

    IF engage < ADD_MONTHS(sysdate,-12)
    THEN sal_year := salaire * 13
    ;
    ELSE sal_year := salaire * 13 + (salaire*((sysdate - engage)/100))
    ;
    END IF;
    RETURN sal_year;
  END revenuAnnuelPromoAnc;

END GestionFinanciere;
/

/*14. Testez l'exécution d'une procédure stockée de ce package.*/
exec GestionFinanciere.afficheSalEmp(7369);

/*15. Donnez à un autre étudiant le droit d'exécuter le package et vérifiez qu'il peut exécuter une procédure.*/

GRANT EXECUTE
ON GestionFinanciere
TO [étudiant]
;

/*16. Créez la table mes_augmentations(empno : number(5), date_aug : date, montant_aug : number(6,2)) qui permettra de garder un historique des augmentations.*/

CREATE TABLE mes_augmentations(
  EMPNO number(5) NOT NULL,
  DATE_AUG date NULL,
  MONTANT_AUG number(6,2) NULL,
  PRIMARY KEY (EMPNO),
  FOREIGN KEY (EMPNO) REFERENCES emp
);

/*17. Ecrire un déclencheur qui permet de d'alimenter la table mes_augmentations en cas d'augmentation d'un employé.*/

SET SERVEROUTPUT ON

CREATE OR REPLACE TRIGGER maj_augmentations 
AFTER UPDATE OF sal 
ON emp FOR EACH ROW 
BEGIN
   INSERT INTO mes_augmentations(empno, date_aug, montant_aug) 
   VALUES(:OLD.empno, SYSDATE, :NEW.sal - :OLD.sal);
END;
/