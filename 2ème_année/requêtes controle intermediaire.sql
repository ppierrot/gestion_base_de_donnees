CREATE TABLE temps_projet
( 
  projet number(7) NOT NULL,
  employe number(5) NOT NULL,
  debsemaine date NOT NULL,
  tache number(2) NOT NULL,
  nbheures number(2) NOT NULL,
  CONSTRAINT fk_projet
    FOREIGN KEY (projet)
    REFERENCES projet (nump),
  CONSTRAINT fk_employe
    FOREIGN KEY (employe)
    REFERENCES employe (nume),
  CONSTRAINT fk_tache
    FOREIGN KEY (tache)
    REFERENCES type_tache (numt),
  CONSTRAINT ck_nbheures CHECK (nbheures between 1 and 40),
  CONSTRAINT pk_projet PRIMARY KEY (projet, employe, debsemaine, tache),
);


select nump, intitule, libelle, sum(nbheures) 
from projet p join temps_projet tp on p.nump = tp.projet join type_tache tt on tp.tache = tt.numt
group by nump, numt, intitule, libelle 
having sum(nbheures) = (select  max(sum(nbheures)) 
                        from temps_projet
                        group by projet, tache)            
;

select distinct chef
from projet
where  datedeb < ADD_MONTHS(sysdate, -2) 
