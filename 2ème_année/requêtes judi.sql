1/
select noclient, nom, prenom, ville 
from clients 
where lower(nom) like 't%';

2/
select nocmd, noclient, datecommande, etatcommande
from cmd
where datecommande = to_date('05-2009', 'MM-YYYY')
order by noclient desc, datecommande asc;
-- order by 2 desc, 3 asc;
-- order by noclient, datecommande desc;
 
(in pour plusieurs résultats, = pour un seul) 
3/
select count(1)
from clients
where ville = 'Paris'
and noclient in (select noclient 
                 from cmd 
                 where datecommande 
                 > to_date('12/10/2008', 'DD/MM/YYYY'
                )
;

4/
select nocmd, nom, noclient, quantite
from cmd_livres
where nolivre = (select nolivre
                 from livres
                 where titre = 'Le seigneur des anneaux'
                )
order by nocmd, noclient
;

5/
select sum(quantite)
from cmd_lignes cl join cmd c on cl.nocmd = c.nocmd join clients ct on c.noclient = ct.noclient
where to_char(datecommande, 'YYYY') = '2007'
group by pays
;

6/
Numéros des clients ayant commandé plus de 3000 ouvrages en tout (toutes leurs commandes confondues), 
et la quantité effectivement commandée.

select noclient, sum(quantite)
from clients ct join cmd c on ct.noclient = c.noclient join cmd_lignes cl on c.nocmd = cl.nocmd
GROUP BY c.noclient
HAVING sum(cl.quantite)>3000

7/
Noms des auteurs qui ne sont édités que chez Pearson (de deux façons, une corrélée et l'autre non. 
Vérifiez avec les outils vus en cours si une requête est plus efficace que l'autre). 
Attention, pour certains livres, l'éditeur n'est pas renseigné.

select distinct auteur
from livres l1
where editeur = 'Pearson'
and not exists (select * 
            from livres l2
            where l1.auteur = l2.auteur
            and l2.editeur != 'Pearson')

/créer view
create view v_clientsParis as
select noclient, nom
from clients
where ville = 'Paris'

select constraint_name from all_constraints where constraint_name like "s%"